#!/bin/bash
FILE=$1
COUNTER=0
declare -A dictionary
declare -A array
declare -A delays
declare -A num_delays
declare -A downloads
MAX_ID=0

grep "@" $FILE > tmp
FILE=tmp
#echo "preparing"
while read line1
do
#	echo line:$line1
#	echo loop
	line=`echo $line1 | grep "@EXECUTING_LOCALLY"`
	if [  -n "$line" ]
	then
		ID=`echo $line | cut -d ' ' -f 2`
		if (( $ID > $MAX_ID ))
		then
			MAX_ID=$ID
		fi

		KERNEL=`echo $line | cut -d '|' -f 2-`
#		echo "$ID -> $KERNEL"
		if [ -z ${dictionary[$KERNEL]} ]
		then
			echo new kernel!
			dictionary[$KERNEL]=$COUNTER
			let COUNTER+=1
		fi
		index=${dictionary[$KERNEL]}
		
		if [ -z ${array[${ID},${index}]} ]
		then
			array[${ID},${index}]=1
		else
			let array[${ID},${index}]+=1
		fi
	fi
	#@DELAY|/example/1|152|@ON_TIME
	line=`echo $line1 | grep "@DELAY"`
#	echo "line after echoing delay $line"
	if [  -n "$line" ]
	then
		KERNEL=`echo $line | cut -d '|' -f 2`
		DELAY=`echo $line | cut -d '|' -f 3`
		echo $line
		echo "adding delay to $KERNEL ($DELAY)"
		if [ -z ${delays[${KERNEL}]} ]
		then
#			echo "new"
			let delays[${KERNEL}]=${DELAY}
			let num_delays[${KERNEL}]=1
		else	
#			echo "second"
			let delays[${KERNEL}]+=${DELAY}
			let num_delays[${KERNEL}]+=1
		fi
		let delays[${KERNEL}]-=100
		echo new val ${delays[${KERNEL}]}
		
	fi

	line=`echo $line1 | grep "@Requesting kernel|"`
#	echo "line after echoing delay $line"
	if [  -n "$line" ]
	then
		KERNEL=`echo $line | cut -d '|' -f 2`
		if [ -z ${downloads[${KERNEL}]} ]
                then
                        let downloads[${KERNEL}]=1
                else
                        let downloads[${KERNEL}]+=1
                fi
	fi



done < $FILE

executed_locally=`grep "@EXECUTING_LOCALLY" $FILE | wc -l`
executed_cloud=`grep "@EXECUTING_CLOUD" $FILE | wc -l`
echo "Executed locally: $executed_locally, executed in the cloud: $executed_cloud"
overloaded=`grep "@OVERLOADED" $FILE  | wc -l`
no_kernel=`grep "@NO_KERNEL" $FILE  | wc -l`
echo "Overloded: $overloaded, no kernel: $no_kernel"

echo "Max id: $MAX_ID"
echo "Dictionary:"
for kernel in "${!dictionary[@]}"
do 
	echo "$kernel - ${dictionary[$kernel]}"
done

echo "Delays:"
for kernel in "${!dictionary[@]}"
do 
	AVG=`bc -l <<< "${delays[${kernel}]} / ${num_delays[${kernel}]}"`
	echo "$kernel - ${delays[${kernel}]} - ${num_delays[${kernel}]} - $AVG"
done


echo "Fairness"

echo "Downloads:"
for kernel in "${!dictionary[@]}"
do 
	echo "$kernel - ${downloads[${kernel}]}"
done

echo "Heatmap:"
#exit

for ((i = 0; i <= MAX_ID; i++)) do
	SUM=0
	for ((j = 0; j < COUNTER; j++)) do
		if [ -z ${array[$i,$j]} ]
		then
			echo -n "0 "
		else
			let SUM+=${array[$i,$j]}
			echo -n "${array[$i,$j]} "
		fi
	done
	echo
	SUMS[${i}]=$SUM
done
echo differently:
for entry in "${!SUMS[@]}"; do echo "$entry - ${SUMS[$entry]}"; done
