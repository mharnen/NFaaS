#for pop in 2 5 10 15 20
for pop in 2 5 25
do
	sed -i "s/POP_SIZE = .*;/POP_SIZE = ${pop};/g" ./src/ndnSIM/NFD/daemon/table/kernel-info.cpp

	#for period in 1 2 5 10 15 20
	for period in 5 20
	do
		sed -i "s/PERIOD_SIZE = .*;/PERIOD_SIZE = ${period};/g" ./src/ndnSIM/NFD/daemon/table/kernel-info.cpp

#		for kernels in 1 2 3 4 5
		for kernels in 2 18
		do
			#set KS size
			let size=$kernels/2
			sed -i "s/MAX_STORED_KERNELS = .*;/MAX_STORED_KERNELS = ${size};/g" ./src/ndnSIM/NFD/daemon/table/ks.hpp

			for stability in 100
			do
				sed -i "s/#define STABILITY .*$/#define STABILITY ${stability}/g" ./src/ndnSIM/NFD/daemon/table/kernel-info.cpp
				filename="pop${pop}period${period}kernels${kernels}stability${stability}"

				echo "running $filename"
				NS_LOG=nfd.KernelStore:nfd.KernelInfo:Simulation:ndn.Consumer ./waf --run="ndn-function --format=Inet --input=./src/topology-read/examples/task-topo.txt --deadline=300 --size=100 --frequency=1 --kernels=${kernels} --time=${time}"  &> ${filename}.log
			
				./print_score.sh ${filename}.log ${kernels} ${pop} ${period}
				mv output.pdf ${out_dir}/${filename}.pdf
			done

		done
		
	done
done

