#!/bin/bash
FILE=$1
MAX_ID=$2
declare -a score
#echo "~Score:/example/2:58 -30 20 48" | cut -d ':' -f 2 | cut -d '/' -f 3
COUNTER=0
TIME=0
while read line1 
do
	line=`echo $line1 | grep "~Score"`
	#extract score
	if [  -n "$line" ]
	then
#		echo line: $line
		KERNEL=`echo $line | cut -d '~' -f 2 | cut -d ':' -f 2 | cut -d '/' -f 3`
		SCORE=`echo $line | cut -d '~' -f 2 | cut -d ':' -f 3 | cut -d ' ' -f 4`
		echo kernel $KERNEL score $SCORE
		let score[${KERNEL}]=${SCORE}


	fi

	line=`echo $line1 | grep "s 0 "`
#	echo "line after echoing delay $line"
	if [  -n "$line" ]
	then
		TIME=`echo $line | cut -d 's' -f 1`
	fi		

	line=`echo $line1 | grep "~Separator"`
#	echo "line after echoing delay $line"
	if [  -n "$line" ]
	then
		echo -ne "$TIME\t"	
		for i in $(seq 1 $MAX_ID) 
		do
			if [ -z ${score[${i}]} ]
			then
				echo -ne "0\t"
			else
				echo -ne "${score[${i}]}\t"
			fi
		done
		echo
		let COUNTER+=1
	fi
done < $FILE
