#include "face-info.hpp"


namespace nfd{
NFD_LOG_INIT("FaceInfo");
namespace ks{
FaceInfo::FaceInfo()
{
}

FaceInfo::FaceInfo(FaceId faceId)
{
  m_faceId = faceId;
}

void
FaceInfo::updateDelay(unsigned int delay) const{
  if(m_delayNum && (delay > (m_sumDelay/m_delayNum)*1.1)){
	m_overloaded = true;
  }else{
  	m_overloaded = false;
  }
  m_sumDelay+= delay;
  m_delayNum++;
  if(m_delayNum >= 10){ m_sumDelay /= 2; m_delayNum /= 2;}

}
void
FaceInfo::updatePopularity(const unsigned long packetCounter, FaceId faceId) const{ 
NS_LOG_DEBUG("updatePopularity(" << packetCounter << ", " << faceId << ")" << "lastPacket: " << m_lastPacket << "periodSize: " << PERIOD_SIZE);
                                                                                                      
        if( (!(packetCounter % PERIOD_SIZE) || (m_popularity.size() == 0))  ){                                          
		NS_LOG_DEBUG("adding new row");                                                       
                m_lastPacket += PERIOD_SIZE;                                                           
                m_popularity.push_back(0);                                                              
                NS_LOG_DEBUG(*this);                                                                  
                if(m_popularity.size() > POP_SIZE){                                                     
                        NS_LOG_DEBUG("popping");                                                      
                        m_popularity.pop_front();
                }
        }                                                                                             
                                                                                                      
        if(faceId == m_faceId){                                                                      
                NFD_LOG_DEBUG("my id ++");                                                          
                m_popularity.back()++;                                                                  
        }else{
                NFD_LOG_DEBUG("not my id "  << m_faceId << " " << faceId);                              
        }
                                                                                                      
} 
  


std::ostream &operator<<(std::ostream &os, nfd::ks::FaceInfo f) {
  os << "FaceId: " << f.m_faceId << " m_sumDelay: " << f.m_sumDelay << " m_delayNum: " << f.m_delayNum << " ->";
  std::list<int>::reverse_iterator rit; 
  for (rit = f.m_popularity.rbegin(); rit != f.m_popularity.rend(); ++rit) {         
    os << *rit << "/" << f.PERIOD_SIZE << "|";                                 
  }
  return os;
}


}
}
