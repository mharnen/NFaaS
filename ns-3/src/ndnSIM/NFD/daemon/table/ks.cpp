#include "ks.hpp"
#include "ns3/simulator.h"
#include "ns3/core-module.h"
#include <signature.hpp>

#include "ns3/ndnSIM/model/ndn-common.hpp"
#include "ns3/ndnSIM/model/ndn-app-link-service.hpp"
#include "ns3/ndnSIM/NFD/daemon/face/face.hpp"
#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-global-routing-helper.hpp"
#include "ns3/node.h"

NFD_LOG_INIT("KernelStore");

#define SCORE_BASE 20
#define SPECILIZE 1
#define QUEUE_SIZE 0
namespace nfd {
namespace ks {

Ks::Ks() :
		m_cloud(false), m_caching(true) {
}
/*
 * Look for requested kernel and try to run it locally if present
*/
void Ks::find(const Interest& interest, const HitCallback& hitCallback,
		const MissCallback& missCallback) {

	Name kernelName(interest.getName().getSubName(1).getPrefix(-1));
	NS_LOG_DEBUG(
			"Looking for kernel " << interest.getName().getSubName(1).getPrefix(-1));
	if (!m_caching) {
		NS_LOG_DEBUG("Kernel caching disabled");
		if (m_cloud) {
			NFD_LOG_DEBUG("I'm the cloud - executing with a delay");
			NS_LOG_ERROR("@EXECUTING_CLOUD|"<<kernelName);
			ns3::Simulator::Schedule(
					ns3::MilliSeconds(interest.getTaskSize() + 100),
					&nfd::ks::Ks::sendData, this, interest, hitCallback);
			return;
		}
		missCallback(interest);
		return;
	}
	/*bool
	 Ks::runKernel(Kernel& kernel){*/
	KernelInfo kernelInfo(kernelName);
	//auto kernelInfo = make_shared<KernelInfo> (kernelName);
	std::vector<KernelInfo>::iterator it;
	// it = m_storedKernels.find(kernelInfo);
	for (it = m_storedKernels.begin(); it != m_storedKernels.end(); it++) {
		if (it->getName().equals(kernelName))
			break;
	}
	if (it != m_storedKernels.end()) {
		NFD_LOG_DEBUG("Kernel present in the KS " << kernelName);
		if (m_runKernels.size() < MAX_RUN_KERNELS) {
			NFD_LOG_DEBUG(
					"Can run the kernel " << kernelName << " already running " << m_runKernels.size() << " kernels");
			m_runKernels.push_back(*it);
			NS_LOG_ERROR(
					"@EXECUTING_LOCALLY|"<< interest.getName().getSubName(1).getPrefix(-1));

			//NFD_LOG_DEBUG(*this);
			ns3::Simulator::Schedule(ns3::MilliSeconds(interest.getTaskSize()),
					&nfd::ks::Ks::sendData, this, interest, hitCallback);
			if (SPECILIZE && (m_packetsWithoutDelay++ > 10)
					&& (CURRENT_STORED_KERNELS < MAX_STORED_KERNELS)) {
				NS_LOG_DEBUG(
						"Increasing store size to: " << CURRENT_STORED_KERNELS);
				CURRENT_STORED_KERNELS++;
				m_packetsWithoutDelay = 0;
			}

			return;
		} else {
			NFD_LOG_DEBUG(
					"QUEUE_SIZE " << QUEUE_SIZE << " m_queue.size() = " << m_queue.size());
			if (m_queue.size() < QUEUE_SIZE) {
				NS_LOG_DEBUG("Queueing kernel " << kernelName);
				m_queue.push_back(interest);
				return;
			}
			NFD_LOG_DEBUG("Overloaded - Can't run the kernel " << kernelName);
			NS_LOG_ERROR("@OVERLOADED:" <<kernelName);
			/*if (CURRENT_STORED_KERNELS > 1) {
				CURRENT_STORED_KERNELS--;
				NS_LOG_DEBUG(
						"Reducing store size to: " << CURRENT_STORED_KERNELS);
			}*/
			m_packetsWithoutDelay = 0;
		}

	} else {
		NFD_LOG_DEBUG("Kernel not present in the KS");
		NS_LOG_ERROR("@NO_KERNEL:" << kernelName);
		if (SPECILIZE && (CURRENT_STORED_KERNELS < MAX_STORED_KERNELS))
			CURRENT_STORED_KERNELS++;
	}
	if (m_cloud) {
		NFD_LOG_DEBUG("I'm the cloud - executing with a delay");
		NS_LOG_ERROR("@EXECUTING_CLOUD|"<<kernelName);
		ns3::Simulator::Schedule(ns3::MilliSeconds(interest.getTaskSize() + 50),
				&nfd::ks::Ks::sendData, this, interest, hitCallback);
		return;
	}
	missCallback(interest);
}

/*
 * Send back data packet with function result
*/
void Ks::sendData(const Interest& interest, const HitCallback& hitCallback) {
	Name dataName(interest.getName());
	auto data = make_shared<ndn::Data>();
	data->setName(dataName);
	data->setContent(make_shared<::ndn::Buffer>(256));

	ndn::Signature signature;
	ndn::SignatureInfo signatureInfo(
			static_cast<::ndn::tlv::SignatureTypeValue>(255));

	/*    if (m_keyLocator.size() > 0) {
	 signatureInfo.setKeyLocator(m_keyLocator);
	 }*/

	signature.setInfo(signatureInfo);
	signature.setValue(
			::ndn::makeNonNegativeIntegerBlock(::ndn::tlv::SignatureValue, 0));

	data->setSignature(signature);

	Name kernelName(interest.getName().getSubName(1).getPrefix(-1));
	NFD_LOG_DEBUG("Removing run kernel " << kernelName);
	std::vector<KernelInfo>::iterator it;
	for (it = m_runKernels.begin(); it != m_runKernels.end(); ++it) {
		if (it->getName().equals(kernelName))
			break;
	}

	NS_ASSERT((m_cloud) || (it != m_runKernels.end()));
	if (it != m_runKernels.end())
		m_runKernels.erase(it);

	NFD_LOG_DEBUG("Sending back data: " << *data);
	hitCallback(interest, *data);

	if (!m_queue.empty()) {
		Interest qInterest = m_queue.front();
		m_queue.pop_front();
		Name qKernelName(qInterest.getName().getSubName(1).getPrefix(-1));
		NS_LOG_DEBUG("Running a kernel from the queue " << qKernelName);
		KernelInfo qKernelInfo(qKernelName);
		std::set<KernelInfo, comp_kernel_infos>::iterator qit;
		qit = m_kernelInfos.find(qKernelInfo);

		m_runKernels.push_back(*qit);
		ns3::Simulator::Schedule(
				ns3::MilliSeconds(qInterest.getTaskSize() + 50),
				&nfd::ks::Ks::sendData, this, qInterest, hitCallback);
	}

}

void Ks::managePopularity(const Interest& interest, const int packetCounter,
		int hopCount) {
	NS_LOG_FUNCTION_NOARGS();
	std::set<KernelInfo>::iterator it;
	Name kernelName(interest.getName().getSubName(1).getPrefix(-1));

	KernelInfo kernelInfo(kernelName, interest.getTaskDeadline());
	if (interest.getTaskDeadline() < 100) {
		kernelInfo.setTaskType(0);
	} else {
		kernelInfo.setTaskType(1);
	}
//	auto kernelInfo = make_shared<KernelInfo> (kernelName);
	it = m_kernelInfos.find(kernelInfo);
//	NS_LOG_DEBUG("Looking for " << kernelName);
//	NS_LOG_DEBUG(*this);
	if (it == m_kernelInfos.end()) {
//		NS_LOG_DEBUG("New kernel");
		m_kernelInfos.insert(kernelInfo);
	}
//	NS_LOG_DEBUG("Updating popularity");
	for (it = m_kernelInfos.begin(); it != m_kernelInfos.end(); it++) {
		it->updatePopularity(packetCounter, kernelName, hopCount);
	}

	for (it = m_kernelInfos.begin(); it != m_kernelInfos.end(); it++) {
		NS_LOG_ERROR("~Score:" << it->getName() << ":" << it->calculateScore());
	}
	NS_LOG_ERROR("~Separator");

	updateStore();
}

void Ks::manageFacePopularity(const Interest& interest, const int packetCounter,
		FaceId faceId) {
	//NS_LOG_FUNCTION_NOARGS();

//	NS_LOG_DEBUG("packet " << interest.getName() << " sent on face " << faceId);
	std::set<FaceInfo>::iterator fit;
	for (fit = m_faceInfos.begin(); fit != m_faceInfos.end(); fit++) {
		fit->updatePopularity(packetCounter, faceId);
	}

	std::set<KernelInfo>::iterator kit;
	Name kernelName(interest.getName().getSubName(1).getPrefix(-1));
	KernelInfo kernelInfo(kernelName);
	kit = m_kernelInfos.find(kernelInfo);
	if (kit != m_kernelInfos.end()) {
		kit->updateFacePopularity(faceId);
	}
}

/*
 * Return a best face to forward a request 
*/
FaceId Ks::getBestFace(FaceId faceId, const Interest& interest) {
	NS_LOG_DEBUG("getting best face");
	Name kernelName(interest.getName().getSubName(1).getPrefix(-1));
	std::set<KernelInfo>::iterator it;
	it = m_kernelInfos.find(kernelName);
	if (it == m_kernelInfos.end()) {
		NS_LOG_DEBUG("Unknown kernel " << kernelName);
		return 0;
	}
	NS_LOG_DEBUG("Found kernel " << *it);
	FaceId bestFace = it->getNextFace();

	return bestFace;
}

bool Ks::faceOverloaded(FaceId faceId) {
	std::set<FaceInfo>::iterator fit;
	for (fit = m_faceInfos.begin(); fit != m_faceInfos.end(); fit++) {
		if (faceId == fit->getFaceId()) {
			return fit->m_overloaded;
		}
	}
	return false;
}

/*
 * When data is received on a face, update statistics for the face/kernel name combination
 * This information is later used to determine faces with the lowest delay.
 */
void Ks::manageDelay(const Name name, FaceId faceId, ns3::Time delay) {

	NS_LOG_FUNCTION_NOARGS();
	Name kernelName(name.getSubName(1).getPrefix(-1));
	std::set<KernelInfo>::iterator it;
	for (it = m_kernelInfos.begin(); it != m_kernelInfos.end(); it++) {
		if (it->getName().equals(kernelName)) {
			if (delay.GetMilliSeconds() < it->getDeadline()) {
				it->addFace(faceId);
			}
			it->updateDelay(delay.GetMilliSeconds(), faceId);
			if (delay.GetSeconds() >= 2) {
				it->faceExpired(faceId);
			}
			NS_LOG_DEBUG("Updated kernel info: " << *it);
			break;
		}
	}
	std::set<FaceInfo>::iterator fit;
	for (fit = m_faceInfos.begin(); fit != m_faceInfos.end(); fit++) {
		if (faceId == fit->getFaceId()) {
			fit->updateDelay(delay.GetMilliSeconds());
			NS_LOG_DEBUG("Updated face info: " << *fit);
		}
	}
}

/*
 * Update statistic when a request times out
*/
void Ks::manageDelay(const Name name, ns3::Time delay) {
	NS_LOG_FUNCTION_NOARGS();
	Name kernelName(name.getSubName(1).getPrefix(-1));
	std::set<KernelInfo>::iterator it;
	for (it = m_kernelInfos.begin(); it != m_kernelInfos.end(); it++) {
		NS_LOG_DEBUG("Checking kernel name " << it->getName());
		if (it->getName().equals(kernelName)) {
			NS_LOG_DEBUG("matching");
			std::list<FaceKernelInfo>::const_iterator fit;
			std::list<FaceKernelInfo> tmp;
			tmp = it->m_prefFaces;
			for (fit = tmp.begin(); fit != tmp.end(); fit++) {
				NS_LOG_DEBUG("updating delay for face " << fit->getFaceId());
				manageDelay(name, fit->getFaceId(), delay);
			}
			return;
		}
	}
}
void Ks::updateFace(FaceId faceId) {
	std::set<FaceInfo>::iterator it;
	for (it = m_faceInfos.begin(); it != m_faceInfos.end(); it++) {
		if (faceId == it->getFaceId())
			return;
	}
	FaceInfo faceInfo(faceId);
	m_faceInfos.insert(faceInfo);
}


/*
 * Kernel comparator to use in collections
*/
bool myfunction(KernelInfo k1, KernelInfo k2) {
	return (k1.calculateScore() > k2.calculateScore());
}

bool cmpNames(KernelInfo k1, KernelInfo k2) {
	return (k1.getName().toUri() > k2.getName().toUri());
}

void Ks::managePrefixAdvertisement() {
	std::map<const ndn::RegisteredPrefixId*, ndn::Name>::iterator pit;
	for (pit = m_advertisedPrefix.begin(); pit != m_advertisedPrefix.end();
			pit++) {
		stopAdvertisePrefix(pit->first, pit->second);
	}

	m_advertisedPrefix.clear();

    //need to schedule this just after "now". Scheduling "now" results in errors
	ns3::Simulator::Schedule(ns3::NanoSeconds(1), &nfd::ks::Ks::finishAdd,
			this);

}

void Ks::finishAdd() {
	std::vector<KernelInfo>::iterator vit;
	for (vit = m_storedKernels.begin(); vit != m_storedKernels.end(); vit++) {
		advertisePrefix(vit->getName());
	}
	ns3::Simulator::Schedule(ns3::NanoSeconds(1), &nfd::ks::Ks::calculateTables, this);
	printLocalPrefixes();

}

void Ks::calculateTables(){
	ns3::ndn::GlobalRoutingHelper::CalculateRoutes();
}

void Ks::printLocalPrefixes() {
	return;
	NFD_LOG_DEBUG("Local prefixes (" << m_gr->m_id << ")");
	std::list<shared_ptr<Name>> localPrefixes = m_gr->GetLocalPrefixes();
	std::list<shared_ptr<Name>>::iterator it;
	for (it = localPrefixes.begin(); it != localPrefixes.end(); it++) {
		NFD_LOG_DEBUG((**it));
	}
	NFD_LOG_DEBUG("end");
}

/*
 * Sort kernels regarding their score. We want to keep those with the highest score and remove those that are less useful (lower score).
 */
void Ks::updateStore() {
	NS_LOG_FUNCTION_NOARGS();
	//NS_LOG_DEBUG(*this);

	//NS_LOG_DEBUG("Caching: " << m_caching);
	if (!m_caching) {
		NS_LOG_DEBUG("I'm not caching - return");
		return;
	}

	//NS_LOG_DEBUG("Before: " << *this);
	std::vector<KernelInfo> newKernels;
	KernelInfo tmp(Name(""));
	newKernels.clear();
	//setting the size
	int size =
			(CURRENT_STORED_KERNELS > m_kernelInfos.size()) ?
					(m_kernelInfos.size()) : (CURRENT_STORED_KERNELS);
	for (int i = 0; i < size; i++) {
		newKernels.push_back(tmp);
	}
	std::partial_sort_copy(m_kernelInfos.begin(), m_kernelInfos.end(),
				newKernels.begin(), newKernels.end(), myfunction);

	/*NS_LOG_DEBUG("m_kernelInfos size:" << m_kernelInfos.size());
	NS_LOG_DEBUG("new kernels size:" << newKernels.size());
	NS_LOG_DEBUG("m_storedKernels:" << m_storedKernels.size());*/
	//NS_LOG_DEBUG(*this);

	std::sort(newKernels.begin(), newKernels.end(), comp_kernel_infos());
	std::sort(m_storedKernels.begin(), m_storedKernels.end(), comp_kernel_infos());

	std::vector<KernelInfo> diff;
	std::set_difference(newKernels.begin(), newKernels.end(), m_storedKernels.begin(), m_storedKernels.end(),
	                        std::inserter(diff, diff.begin()), cmpNames);

	/*NS_LOG_DEBUG("newKernels size:" << newKernels.size());
	std::vector<KernelInfo>::iterator nit;
	for(nit = newKernels.begin(); nit != newKernels.end(); nit++){
		NS_LOG_DEBUG(nit->m_name << " ");
	}

	NS_LOG_DEBUG("storedKernels size:" << m_storedKernels.size());
	std::vector<KernelInfo>::iterator skit;
	for(skit = m_storedKernels.begin(); skit != m_storedKernels.end(); skit++){
		NS_LOG_DEBUG(skit->m_name << " ");
	}*/

	//NS_LOG_DEBUG("diff size:" << diff.size());
	std::vector<KernelInfo>::iterator dit;
	for(dit = diff.begin(); dit != diff.end(); dit++){
		//NS_LOG_DEBUG(dit->m_name << " ");
		requestKernel(dit->m_name);
	}

	m_storedKernels = newKernels;

	NS_LOG_DEBUG(*this);

	//managePrefixAdvertisement();

	std::set<KernelInfo>::iterator sit;
	std::vector<KernelInfo>::iterator vit;
	std::set<KernelInfo, nfd::ks::comp_kernel_infos> tmpSet;
	for (sit = m_kernelInfos.begin(); sit != m_kernelInfos.end(); sit++) {
		bool modified = false;
		for (vit = m_storedKernels.begin(); vit != m_storedKernels.end();
				vit++) {
			if (sit->m_name.equals(vit->m_name)) {
				NS_LOG_DEBUG(sit->m_name << " is stored");
				sit->m_stored = true;
				modified = true;
				break;
			}
		}
		if (!modified){
			sit->m_stored = false;
			NS_LOG_DEBUG(sit->m_name << " is NOT stored");
		}
	}
}

/*
 * When the KS determines a popular kernel that we don't have - it can be requested by sending an Interest
 */
void Ks::requestKernel(const Name& kernelName) {
	NS_LOG_DEBUG("@Requesting kernel|" << kernelName);

	return;
	NS_LOG_DEBUG("Requesting kernel" << kernelName);
	/*  shared_ptr<Name> name = make_shared<Name>(kernelName);
	 KernelInfo kernel(*name);
	 this->storedKernel(kernel);
	 */
	/*
	 *   shared_ptr<Name> name = make_shared<Name>(kernelName);
	 *
	 *     shared_ptr<Interest> interest = make_shared<Interest>();
	 *      // interest->setNonce(m_rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
	 *        interest->setName(*name);
	 *
	 *          m_genericLinkService->sendInterest(*interest);
	 *            NS_LOG_DEBUG("Interest for " << kernelName);
	 *
	 *  m_genericLinkService->onReceiveInterest(*interest);

	 m_transmittedInterests(interest, this, m_face);*/
}

void Ks::createFace() {
	NS_LOG_FUNCTION_NOARGS();
	m_face = make_shared<ndn::Face>();
}

void Ks::printFib() {
	return;
	NS_LOG_DEBUG("printing FIB:");
	for (nfd::Fib::const_iterator entry = m_fib->begin(); entry != m_fib->end();
			entry++) {
		const fib::NextHopList& nexthops = entry->getNextHops();
		for (fib::NextHopList::const_iterator it = nexthops.begin();
				it != nexthops.end(); ++it) {
			NS_LOG_DEBUG(entry->getPrefix() << " -> " << it->getFace());
		}
	}
}

void Ks::deleteFib() {
	/*NS_LOG_DEBUG("deleting FIB:");
	nfd::fib::Entry entry;
	Name name("/example/");
	while ( (entry = m_fib->findLongestPrefixMatch(name)) != nullptr){
		NFD_LOG_DEBUG("Deleting entry: " <<entry.getPrefix());
		m_fib->erase(entry);
	}*/

}


/*
 * If the node is the first one to receive download a kernel it becomes the main node for this function and advertises its prefix. 
 * It means that other nodes will push requests towards this node, but still any other, intermediary  node can run this kernel. 
 */
bool Ks::advertisePrefix(Name name) {
	NS_LOG_FUNCTION_NOARGS();

	if (!m_caching)
		return false;

	if (m_face == NULL)
		createFace();

	if (m_fib->findExactMatch(name) == nullptr) {
		NS_LOG_DEBUG("Prefix not registered - registering " << name);
		const ndn::RegisteredPrefixId* id = m_face->registerPrefix(name,
				std::bind(&nfd::ks::Ks::onLocalhostRegistrationSuccess, this,
						_1),
				std::bind(&nfd::ks::Ks::registrationFailed, this, _1));
		m_advertisedPrefix.insert(
				std::pair<const ndn::RegisteredPrefixId*, Name>(id, name));
		auto prefix = make_shared<Name>(name);
		m_gr->AddLocalPrefix(prefix);
		printFib();
		return true;
	} else {
		NS_LOG_DEBUG("Prefix already registered - aborting " << name);
		return false;
	}
}

void Ks::onLocalhostRegistrationSuccess(const ndn::Name& name) {
	NS_LOG_DEBUG("Successfully registered prefix: " << name);
}

void Ks::registrationFailed(const ndn::Name& name) {
	NS_LOG_DEBUG("ERROR: Failed to register prefix in local hub's daemon");
}

void Ks::stopAdvertisePrefix(const ndn::RegisteredPrefixId* id,
		ndn::Name name) {
	NS_LOG_DEBUG("Trying to stop advertising: " << id);
	if (m_face == NULL)
		createFace();
	m_face->unregisterPrefix(id, bind([] {NS_LOG_DEBUG("Unregister success");}),
			bind([] {NS_LOG_DEBUG("Unregister failed");}));
	m_gr->DeleteLocalPrefix(name);
}

void Ks::setCloud(bool cloud) {
	m_cloud = cloud;
}

void Ks::setCaching(bool caching) {
	m_caching = caching;
}

std::ostream &operator<<(std::ostream &os, Ks const &k) {
	os << "-------------------------------------------";
	os << "Kernel Store\nKernels Infos:\n";
	std::set<KernelInfo>::iterator iti;
	for (iti = k.m_kernelInfos.begin(); iti != k.m_kernelInfos.end(); iti++) {
		os << *iti << "\n";
	}
	std::vector<KernelInfo>::const_iterator it;
	os << "Stored Kernels:\n";
	for (it = k.m_storedKernels.begin(); it != k.m_storedKernels.end(); ++it) {
		os << "->" << *it << "\n";
	}
	os << "Run Kernels:\n";
	for (it = k.m_runKernels.begin(); it != k.m_runKernels.end(); ++it) {
		os << "->" << *it << "\n";
	}
	os << "Faces:\n";
	std::set<FaceInfo>::iterator fit;
	for (fit = k.m_faceInfos.begin(); fit != k.m_faceInfos.end(); ++fit) {
		os << "->" << *fit << "\n";
	}
	os << "-------------------------------------------";
	return os;
}
} // namespace cs
} // namespace nfd
